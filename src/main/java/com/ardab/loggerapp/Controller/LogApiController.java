package com.ardab.loggerapp.Controller;

import com.ardab.loggerapp.Model.Transaction;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class LogApiController {
    private static final Logger logger = LoggerFactory.getLogger(LogApiController.class);

    @PostMapping("/api/transaction")
    public ResponseEntity<Map<String, String>> transaction(@RequestBody Transaction transaction){
        Map<String, String> response = new HashMap<>();
        response.put("transaction", transaction.getTransactionType());
        response.put("inserted", Boolean.TRUE.toString());
        logger.info("Transaction ID:" +transaction.getTransactionId()+" | Transaction Cost:" +transaction.getTransactionCost()+" | Response:" +response.get("inserted"));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
